package com.nutrons.demo17.commands.Climbtake;

import com.nutrons.demo17.Robot;
import edu.wpi.first.wpilibj.command.Command;

public class ClimbtakeCmd extends Command {

    public ClimbtakeCmd() {
        requires(Robot.climbtake);
    }

    protected void initialize() {

    }

    protected void execute() {
        Robot.climbtake.climbtake();
    }

    protected boolean isFinished() {
        return false;
    }

    protected void end() {
        Robot.climbtake.stopClimbtake();
    }

    protected void interrupted() {
        end();
    }

}