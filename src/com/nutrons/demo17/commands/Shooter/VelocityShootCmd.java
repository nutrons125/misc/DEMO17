package com.nutrons.demo17.commands.Shooter;

import com.nutrons.demo17.Robot;
import edu.wpi.first.wpilibj.command.Command;

public class VelocityShootCmd extends Command {


    public VelocityShootCmd() {
        requires(Robot.shooter);
    }

    protected void initialize() {

    }

    protected void execute() {
        Robot.shooter.shootAtVelocity();
    }

    protected boolean isFinished() {
        return false;
    }

    protected void end() {
        Robot.shooter.stopShooter();
    }

    protected void interrupted() {
        end();
    }

}