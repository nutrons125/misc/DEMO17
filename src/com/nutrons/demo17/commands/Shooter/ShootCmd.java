package com.nutrons.demo17.commands.Shooter;

import com.nutrons.demo17.Robot;
import edu.wpi.first.wpilibj.command.Command;

public class ShootCmd extends Command {

    private double pow;

    public ShootCmd(double pow) {
        requires(Robot.shooter);
        this.pow = pow;
    }

    protected void initialize() {

    }

    protected void execute() {
        Robot.shooter.shoot(pow);
    }

    protected boolean isFinished() {
        return false;
    }

    protected void end() {
        Robot.shooter.stopShooter();
    }

    protected void interrupted() {
        end();
    }

}