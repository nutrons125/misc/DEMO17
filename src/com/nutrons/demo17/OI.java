package com.nutrons.demo17;

import com.nutrons.demo17.commands.Climbtake.ClimbtakeCmd;
import com.nutrons.demo17.commands.Hopper.FeedCmd;
import com.nutrons.demo17.commands.Shooter.ChangeVelocityCmd;
import com.nutrons.demo17.commands.Shooter.VelocityShootCmd;
import com.nutrons.demo17.subsystems.Shooter;
import com.nutrons.demo17.util.JoystickMap;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import com.nutrons.demo17.subsystems.Shooter.*;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {

    //Controllers
    public Joystick driverPad = new Joystick(0);
    public Joystick opPad = new Joystick(1);

    private static final double STICK_DEADBAND = 0.05;

    public Button runClimbtake = new JoystickButton(driverPad, JoystickMap.RB);
    public Button controlledShoot = new JoystickButton(opPad, JoystickMap.LB);
    public Button feed = new JoystickButton(opPad, JoystickMap.RB);
    public Button setPowerVeryLow = new JoystickButton(opPad, JoystickMap.A);
    public Button setPowerLow = new JoystickButton(opPad, JoystickMap.X);
    public Button setPowerMedium = new JoystickButton(opPad, JoystickMap.Y);
    public Button setPowerHigh = new JoystickButton(opPad, JoystickMap.B);

    public OI() {
        runClimbtake.whileHeld(new ClimbtakeCmd());
        controlledShoot.whileHeld(new VelocityShootCmd());
        feed.whileHeld(new FeedCmd());
        setPowerVeryLow.whenPressed(new ChangeVelocityCmd(Velocities.veryLowPow));
        setPowerLow.whenPressed(new ChangeVelocityCmd(Velocities.lowPow));
        setPowerMedium.whenPressed(new ChangeVelocityCmd(Velocities.mediumPow));
        setPowerHigh.whenPressed(new ChangeVelocityCmd(Velocities.highPow));
    }

    private static double stickDeadband(double value, double deadband, double center) {
        return (value < (center + deadband) && value > (center - deadband)) ? center : value;
    }

    public double getDriverLeftStickY() {
        return stickDeadband(this.driverPad.getRawAxis(JoystickMap.LEFT_Y), STICK_DEADBAND, 0.0);
    }

    public double getDriverLeftStickX() {
        return stickDeadband(this.driverPad.getRawAxis(JoystickMap.LEFT_X), STICK_DEADBAND, 0.0);
    }

    public double getDriverRightStickY() {
        return stickDeadband(this.driverPad.getRawAxis(JoystickMap.RIGHT_Y), STICK_DEADBAND, 0.0);
    }

    public double getDriverRightStickX() {
        return stickDeadband(this.driverPad.getRawAxis(JoystickMap.RIGHT_X), STICK_DEADBAND, 0.0);
    }

    public double getDriverTriggerSum() {
        return this.driverPad.getRawAxis(JoystickMap.RIGHT_TRIGGER) - this.driverPad.getRawAxis(JoystickMap.LEFT_TRIGGER);
    }

    public double getOpRightTrigger() {
        return this.opPad.getRawAxis(JoystickMap.RIGHT_TRIGGER);
    }

    public double getOpLeftStickX() {
        return stickDeadband(this.opPad.getRawAxis(JoystickMap.LEFT_X), STICK_DEADBAND, 0.0);
    }
}
