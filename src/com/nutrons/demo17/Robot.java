/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package com.nutrons.demo17;

import com.nutrons.demo17.subsystems.*;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The robot class is responsible for running the robot
 * during matches. It handles everything from creating auto commands and subsystems (including the OI)
 * and deciding what to do as the robot switches between modes. It is the highest level of control that
 * you should be concerned with.
 */
public class Robot extends IterativeRobot {

	public static Drivetrain drivetrain = new Drivetrain();
	public static Climbtake climbtake = new Climbtake();
	public static Hopper hopper = new Hopper();
	public static Shooter shooter = new Shooter();
	public static Turret turret = new Turret();
	public static OI oi;

	//robotInit is ran when the robot is first turned on
	@Override
	public void robotInit() {
		oi = new OI();
	}

	//disabledInit is ran when the robot enters a disabled state
	@Override
	public void disabledInit() {
	}

	//disabledPeriodic is ran repeatedly while the robot is in a disabled state
	@Override
	public void disabledPeriodic() {
		Scheduler.getInstance().run();
	}

	//autonomousInit is ran when the robot enters autonomous mode
	@Override
	public void autonomousInit() {
	}

	//autonomousPeriodic is ran repeatedly while the robot is in autonomous mode
	@Override
	public void autonomousPeriodic() {
		Scheduler.getInstance().run();
	}

	//teleopInit is ran when the robot enters teleop mode
	@Override
	public void teleopInit() {
		updateSmartDash();
	}

	//teleopPeriodic is ran repeatedly while the robot is in teleop mode
	@Override
	public void teleopPeriodic() {
		Scheduler.getInstance().run();
	}

	//testPeriodic is ran repeatedly while the robot is in test mode
	@Override
	public void testPeriodic() {
		Scheduler.getInstance().run();
	}

	public void updateSmartDash(){
		SmartDashboard.putNumber("Shooter Velocity", shooter.getShooterVel());
		SmartDashboard.putNumber("Current Velocity Setpoint", shooter.getDesiredVel());
	}

}
