package com.nutrons.demo17.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.nutrons.demo17.RobotMap;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Hopper extends Subsystem {

    //Speed Controllers, these are the devices that control the power of the motors.
    private TalonSRX rotor = new TalonSRX(RobotMap.TOP_HOPPER_MOTOR);
    private TalonSRX rollers = new TalonSRX(RobotMap.SPIN_FEEDER_MOTOR);

    private static final double ROTOR_HI_POWER = 1.0;
    private static final double ROTOR_LOW_POWER = -ROTOR_HI_POWER;
    private static final double ROLLER_HI_POWER = 1.0;
    private static final double ROLLER_LOW_POWER = -ROLLER_HI_POWER;
    private static final double ROTOR_PO = 0.70;
    private static final double ROLLER_PO = 1.0;


    public Hopper() {
        //Configuring the minimum power and maximum power that can be supplied to each motor. Just a bounds thing.
        this.rotor.configPeakOutputForward(ROTOR_HI_POWER, 0);
        this.rotor.configPeakOutputReverse(ROTOR_LOW_POWER, 0);
        this.rotor.configNominalOutputForward(0.0, 0);
        this.rotor.configNominalOutputReverse(0.0, 0);
        this.rollers.configPeakOutputForward(ROLLER_HI_POWER, 0);
        this.rollers.configPeakOutputReverse(ROLLER_LOW_POWER, 0);
        this.rollers.configNominalOutputForward(0.0, 0);
        this.rollers.configNominalOutputReverse(0.0, 0);

        //This is here for quick inversion of either one of the systems
        this.rotor.setInverted(false);
        this.rollers.setInverted(false);
    }

    public void feed() {
        this.rotor.set(ControlMode.PercentOutput, ROTOR_PO);

    }

    public void roll() {
        this.rollers.set(ControlMode.PercentOutput, ROLLER_PO);

    }

    public void stopFeed() {
        this.rotor.set(ControlMode.PercentOutput, 0.0);

    }

    public void stopRoll() {
        this.rollers.set(ControlMode.PercentOutput, 0.0);
    }

    @Override
    protected void initDefaultCommand() {
    }

}
