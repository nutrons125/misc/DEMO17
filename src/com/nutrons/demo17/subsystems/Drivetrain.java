package com.nutrons.demo17.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.nutrons.demo17.RobotMap;
import com.nutrons.demo17.commands.Drivetrain.DriveArcadeCmd;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * The drivetrain class is what allows the robot to move.
 * In this specific iteration the robot has 4 drive motors with 2 followers and 2 leaders.
 * Also implemented is a open loop ramping system for a smoother driving experience and break mode.
 */
public class Drivetrain extends Subsystem {

    //Speed Controllers, these are the devices that control the power of the motors.
    private TalonSRX leftDriveMain = new TalonSRX(RobotMap.LEFT_DRIVE_MAIN);
    private TalonSRX leftDriveFollower = new TalonSRX(RobotMap.LEFT_DRIVE_FOLLOWER);
    private TalonSRX rightDriveMain = new TalonSRX(RobotMap.RIGHT_DRIVE_MAIN);
    private TalonSRX rightDriveFollower = new TalonSRX(RobotMap.RIGHT_DRIVE_FOLLOWER);

    private static final double HIGH_POW = 1.0;
    private static final double LOW_POW = -HIGH_POW;
    private static final double RAMP_RATE = 0.25;

    public Drivetrain() {
        //Follower Controllers setup, telling the followers to mimic their leaders.
        this.leftDriveFollower.follow(leftDriveMain);
        this.rightDriveFollower.follow(rightDriveMain);

        //Configuring the minimum power and maximum power that can be supplied to each motor. Just a bounds thing.
        this.leftDriveMain.configPeakOutputForward(HIGH_POW, 0);
        this.leftDriveMain.configPeakOutputReverse(LOW_POW, 0);
        this.leftDriveMain.configNominalOutputForward(0.0, 0);
        this.leftDriveMain.configNominalOutputReverse(0.0, 0);
        this.leftDriveFollower.configPeakOutputForward(HIGH_POW, 0);
        this.leftDriveFollower.configPeakOutputReverse(LOW_POW, 0);
        this.leftDriveFollower.configNominalOutputForward(0.0, 0);
        this.leftDriveFollower.configNominalOutputReverse(0.0, 0);
        this.rightDriveMain.configPeakOutputForward(HIGH_POW, 0);
        this.rightDriveMain.configPeakOutputReverse(LOW_POW, 0);
        this.rightDriveMain.configNominalOutputForward(0.0, 0);
        this.rightDriveMain.configNominalOutputReverse(0.0, 0);
        this.rightDriveFollower.configPeakOutputForward(HIGH_POW, 0);
        this.rightDriveFollower.configPeakOutputReverse(LOW_POW, 0);
        this.rightDriveFollower.configNominalOutputForward(0.0, 0);
        this.rightDriveFollower.configNominalOutputReverse(0.0, 0);

        //Setting one of the sides of the dt to be inverted since due to how it is built the sides are mirrored.
        this.leftDriveMain.setInverted(false);
        this.leftDriveFollower.setInverted(false);
        this.rightDriveMain.setInverted(true);
        this.rightDriveFollower.setInverted(true);

        //Encoder config.
        this.leftDriveMain.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);
        this.rightDriveMain.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);

        //Calling a method that enables controlled driving which causes dt motors to ramp.
        enableRamping();

        //Calling a method that will stop the motors from continuing to spin when no power is supplied.
        enableBreakMode();
    }

    public void drive(double powLeft, double powRight) {
        this.leftDriveMain.set(ControlMode.PercentOutput, powLeft);
        this.rightDriveMain.set(ControlMode.PercentOutput, powRight);
    }

    public void driveArcade(double throttle, double turn) {
        this.leftDriveMain.set(ControlMode.PercentOutput, throttle + turn);
        this.rightDriveMain.set(ControlMode.PercentOutput, throttle - turn);
    }

    //Enables open loop ramping
    public void enableRamping() {
        this.leftDriveMain.configOpenloopRamp(RAMP_RATE, 0);
        this.leftDriveMain.configClosedloopRamp(RAMP_RATE, 0);
        this.rightDriveMain.configOpenloopRamp(RAMP_RATE, 0);
        this.rightDriveMain.configClosedloopRamp(RAMP_RATE, 0);
    }

    //Disables open loop ramping
    public void disableRamping() {
        this.leftDriveMain.configOpenloopRamp(0.0, 0);
        this.leftDriveMain.configClosedloopRamp(0.0, 0);
        this.rightDriveMain.configOpenloopRamp(0.0, 0);
        this.rightDriveMain.configClosedloopRamp(0.0, 0);
    }

    //Enables break mode in neutral, where neutral is no power supplied.
    public void enableBreakMode() {
        this.leftDriveMain.setNeutralMode(NeutralMode.Brake);
        this.rightDriveMain.setNeutralMode(NeutralMode.Brake);
    }

    //Disables break mode in neutral, where neutral is no power supplied.
    public void disableBreakMode() {
        this.leftDriveMain.setNeutralMode(NeutralMode.Coast);
        this.rightDriveMain.setNeutralMode(NeutralMode.Coast);
    }

    //The default command that the drivetrain does when it is doing no other command.
    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new DriveArcadeCmd());
    }

}
